PRODUCT_COPY_FILES += \
    vendor/firmware/firmware-update/abl.elf:install/firmware-update/abl.elf \
    vendor/firmware/firmware-update/aop.mbn:install/firmware-update/aop.mbn \
    vendor/firmware/firmware-update/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/firmware/firmware-update/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/firmware/firmware-update/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/firmware/firmware-update/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/firmware/firmware-update/dspso.bin:install/firmware-update/dspso.bin \
    vendor/firmware/firmware-update/dtbo.img:install/firmware-update/dtbo.img \
    vendor/firmware/firmware-update/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/firmware/firmware-update/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    vendor/firmware/firmware-update/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
    vendor/firmware/firmware-update/logo.img:install/firmware-update/logo.img \
    vendor/firmware/firmware-update/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/firmware/firmware-update/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/firmware/firmware-update/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/firmware/firmware-update/tz.mbn:install/firmware-update/tz.mbn \
    vendor/firmware/firmware-update/xbl.elf:install/firmware-update/xbl.elf \
    vendor/firmware/firmware-update/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/firmware/firmware-update/vbmeta.img:install/firmware-update/vbmeta.img
